package com.lights.lightbulbchallenge

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView

class LabelAndValue @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private var labelView: AppCompatTextView
    private var valueView: AppCompatTextView

    init {
        val view = inflate(context, R.layout.label_and_value, this)
        labelView = view.findViewById(R.id.label)
        valueView = view.findViewById(R.id.value)
    }

    fun setLabel(label: String) {
        labelView.text = label
    }

    fun setValue(value: String) {
        valueView.text = value
    }

    fun getValueAsInt(): Int {
        return valueView.text.toString().toIntOrNull() ?: 0
    }
}