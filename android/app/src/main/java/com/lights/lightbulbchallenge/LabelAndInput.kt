package com.lights.lightbulbchallenge

import android.content.Context
import android.text.TextWatcher
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView

class LabelAndInput @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private var labelView: AppCompatTextView
    private var inputView: AppCompatEditText

    init {
        val view = inflate(context, R.layout.label_and_input, this)
        labelView = view.findViewById(R.id.input_label)
        inputView = view.findViewById(R.id.input_field)
    }

    fun configure(label: String, defaultInputVal: String) {
        labelView.text = label
        inputView.setText(defaultInputVal)
    }

    fun getInputAsInt(): Int {
        return inputView.text.toString().toIntOrNull() ?: 0
    }

    fun addOnTextChangedListener(watcher: TextWatcher) {
        inputView.addTextChangedListener(watcher)
    }

    fun toggleInput(flag: Boolean) {
        inputView.isEnabled = flag
        inputView.setSelection(0)
    }
}