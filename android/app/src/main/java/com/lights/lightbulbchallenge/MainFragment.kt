package com.lights.lightbulbchallenge

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.*
import kotlin.random.Random

class MainFragment : Fragment(R.layout.main_fragment) {

    private var runBtn: FloatingActionButton? = null
    private var outputView: LabelAndValue? = null
    private var totalBulbsView: LabelAndValue? = null
    private var inputNumColors: LabelAndInput? = null
    private var inputNumPerColor: LabelAndInput? = null
    private var inputNumToPick: LabelAndInput? = null
    private var inputNumSim: LabelAndInput? = null
    private var loadingSpinner: ProgressBar? = null
    private var bulbsBucket = mutableListOf<Int>()
    private var job: Job? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Output and i which is calculated from j * k
        outputView = view.findViewById(R.id.text_output)
        outputView?.setLabel("Total Unique Lightbulbs")
        outputView?.setValue("0")

        totalBulbsView = view.findViewById(R.id.text_total_bulbs)
        totalBulbsView?.setLabel("Total Available Lightbulbs")
        totalBulbsView?.setValue("0")

        // Inputs
        // j
        inputNumColors = view.findViewById(R.id.input_num_colors)
        inputNumColors?.configure("# of Lightbulb Colors", "0")
        // k
        inputNumPerColor = view.findViewById(R.id.input_num_per_color)
        inputNumPerColor?.configure("Quantity per Lightbulb Color", "0")
        // m
        inputNumToPick = view.findViewById(R.id.input_num_to_pick)
        inputNumToPick?.configure("# of Lightbulbs to pick", "0")
        // n
        inputNumSim = view.findViewById(R.id.input_num_sim)
        inputNumSim?.configure("# of Simulations", "1")

        // when either j or k is changed, calculate new value for i
        val bulbChangeWatcher = object : TextWatcher {
            // NO-OP
            override fun afterTextChanged(s: Editable?) {}

            // NO-OP
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            // Recalculate "i" value here
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // i = j * k
                val j = inputNumColors?.getInputAsInt() ?: 0
                val k = inputNumPerColor?.getInputAsInt() ?: 0
                val i = j * k
                totalBulbsView?.setValue(i.toString())
            }
        }
        inputNumColors?.addOnTextChangedListener(bulbChangeWatcher)
        inputNumPerColor?.addOnTextChangedListener(bulbChangeWatcher)

        runBtn = activity?.findViewById(R.id.run_btn)
        runBtn?.setOnClickListener {
            view.hideKeyboard()
            runSimulation()
        }

        loadingSpinner = view.findViewById(R.id.loading_spinner)
    }

    override fun onDestroy() {
        job?.cancel()
        super.onDestroy()
    }

    private fun View.hideKeyboard() {
        val inputManager: InputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun toggleUI(flag: Boolean) {
        inputNumColors?.toggleInput(flag)
        inputNumPerColor?.toggleInput(flag)
        inputNumToPick?.toggleInput(flag)
        inputNumSim?.toggleInput(flag)
        runBtn?.isEnabled = flag
        loadingSpinner?.isVisible = !flag
    }

    private fun runSimulation() {
        val numTimes = inputNumSim?.getInputAsInt() ?: 0
        val numToPick = inputNumToPick?.getInputAsInt() ?: 0
        var avgColors = 0

        // Case where we pick 0 or 1, just return
        // No point in picking for unique colors in this case
        if (numToPick <= 1) {
            outputView?.setValue(numToPick.toString())
            return
        }

        toggleUI(false)
        job = CoroutineScope(Dispatchers.Default).launch {

            for (n in 0 until numTimes) {
                buildBulbsBucket()
                avgColors += chooseBulbs()
            }

            withContext(Dispatchers.Main) {
                toggleUI(true)
                avgColors /= numTimes
                outputView?.setValue(avgColors.toString())
            }
        }
    }

    /*
    Build bucket of light bulbs
    Index -> color
    Value -> quantity
     */
    private fun buildBulbsBucket() {
        val numColors = inputNumColors?.getInputAsInt() ?: 0
        val numPerColor = inputNumPerColor?.getInputAsInt() ?: 0

        bulbsBucket.clear()
        for (color in 0 until numColors) {
            bulbsBucket.add(numPerColor)
        }
    }

    /*
    Pick bulbs from the bucket at random
    Returns # of unique colors in list
     */
    private fun chooseBulbs(): Int {
        val numToPick = inputNumToPick?.getInputAsInt() ?: 0
        val uniqueColorsList = mutableListOf<Int>()

        for (m in 0 until numToPick) {
            if (bulbsBucket.isEmpty() || m >= bulbsBucket.size) break

            val randomColor = Random.nextInt(0, bulbsBucket.size - 1)
            val quantity = bulbsBucket[randomColor]

            // When no more of that color, remove it entirely from the list
            // else, remove the bulb since it's been picked
            if (quantity == 0) {
                bulbsBucket.removeAt(randomColor)
            } else {
                bulbsBucket[randomColor]--;
            }

            if (!uniqueColorsList.contains(randomColor)) {
                uniqueColorsList.add(randomColor)
            }
        }

        return uniqueColorsList.size
    }

}